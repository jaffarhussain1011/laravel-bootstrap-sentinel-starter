<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use APPACL;

class ACLMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions) {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }
        if (APPACL::hasAny($permissions)) {
            return $next($request);
        }
        abort(403);
    }
}
