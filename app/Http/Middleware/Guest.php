<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class Guest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::Check()){
            return Redirect::route('home');
        }
        return $next($request);
    }
}
