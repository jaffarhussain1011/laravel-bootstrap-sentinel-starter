<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use Sentinel;
use View;
use Redirect;
use Session;

class RolesController extends Controller {

    /**
     * Holds the Sentinel Roles repository.
     *
     * @var \Cartalyst\Sentinel\Roles\EloquentRole
     */
    protected $roles;
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct() {
//        parent::__construct();
        $this->roles = Sentinel::getRoleRepository();
    }

    /**
     * Display a listing of roles.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $roles = $this->roles->createModel()->paginate(10);
        return View::make('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new role.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $mode = "create";
        $role = $this->roles->createModel();
        $permissions = Permission::lists('name','name');
        return View::make('roles.create', compact('mode', 'role','permissions'));
    }

    /**
     * Handle posting of the form for creating new role.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(\App\Http\Requests\StoreRolePostRequest $request) {
        $permissionsSelect = $request->get('permissions');
        $permissions = [];
        if(is_array($permissionsSelect)){
        foreach($permissionsSelect as $permission){
            $permissions[$permission]=true;
        }
        }
        $all = array_merge($request->all(),['permissions'=>$permissions]);
        $role = $this->roles->create($all);
        Session::flash('message', 'Created Successfully.');
        return Redirect::to('roles');
    }

    /**
     * Show the form for updating role.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id) {
        $mode = "update";
        $role = $this->roles->createModel()->find($id);
        $permissions = Permission::lists('name','name');
        return View::make('roles.update', compact('mode', 'role','permissions'));
    }

    /**
     * Handle posting of the form for updating role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(\App\Http\Requests\UpdateRolePostRequest $request, $id) {
        $role = $this->roles->createModel()->find($id);
        $permissionsSelect = $request->get('permissions');
        $permissions = [];
        foreach($permissionsSelect as $permission){
            $permissions[$permission]=true;
        }
        $all = array_merge($request->all(),['permissions'=>$permissions]);
        $role->fill($all);
        $role->save();
        Session::flash('message', 'Updated Successfully.');
        return Redirect::to('roles');
    }

    /**
     * Remove the specified role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        if ($role = $this->roles->createModel()->find($id)) {
            $role->delete();
            Session::flash('message', 'Deleted Successfully.');
            return Redirect::to('roles');
        }
        return Redirect::to('roles');
    }
}
