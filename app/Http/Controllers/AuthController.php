<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;
use Activation;
use View;
use Redirect;
use Session;

class AuthController extends Controller {
    
    public function login(Request $request) {
        return View::make('auth.login');
    }

    public function postLogin(\App\Http\Requests\LoginUserPostRequest $request) {
        try {
            $input = $request->all();
            $remember = false;
            if (Sentinel::authenticate($input, $remember)) {
                return Redirect::intended('/');
            }
            $errors = ['IncorrectCredentials' => 'Invalid login or password.'];
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            $errors = ['NotActivated' => 'Account is not activated!'];
            return Redirect::back()
                            ->withInput()
                            ->withErrors($errors);
        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            $delay = $e->getDelay();
            $errors = ['Throttling' => "Your account is blocked for {$delay} second(s)."];
            return Redirect::back()
                            ->withInput()
                            ->withErrors($errors);
        }
        return Redirect::back()
                        ->withInput()
                        ->withErrors($errors);
    }

    public function logout() {
        Sentinel::logout();
        return Redirect::to(route('login'));
    }

}
