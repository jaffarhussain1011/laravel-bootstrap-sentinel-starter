<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;
use Activation;
use View;
use Redirect;
use Session;

class UsersController extends Controller {

    /**
     * Holds the Sentinel Users repository.
     *
     * @var \Cartalyst\Sentinel\Users\EloquentUser
     */
    protected $users;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct() {
//        parent::__construct();

        $this->users = Sentinel::getUserRepository();
    }

    /**
     * Display a listing of users.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $users = $this->users->createModel()->paginate(10);

        return View::make('users.index', compact('users'));
    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $mode = "create";
        $user = $this->users->createModel();
        return View::make('users.create', compact('mode', 'user'));
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(\App\Http\Requests\StoreUserPostRequest $request) {
        $user = $this->users->create($request->all());
        $activation = Activation::create($user);
        $activated = Activation::complete($user, $activation->code);
        Session::flash('message', 'Created Successfully.');
        return Redirect::to('users');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id) {
        $mode = "update";
        $user = $this->users->createModel()->find($id);
        $roles = Sentinel::getRoleRepository()->lists('name', 'id');
        $selecedRoles = array_keys($user->roles()->lists('name', 'id')->toArray());
//        dd($user->roles()->lists('name','slug'));
        return View::make('users.update', compact('mode', 'user', 'roles', 'selecedRoles', 'id'));
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(\App\Http\Requests\UpdateUserPostRequest $request, $id) {
        $user = $this->users->createModel()->find($id);
        $data = $request->except('roles');
        if (empty($data['password'])) {
            unset($data['password']);
        }
        $this->users->update($user, $data);
        if ($id != Sentinel::Check()->id) {
            $user->roles()->sync($request->get('roles'));
        }
        Session::flash('message', 'Updated Successfully.');
        return Redirect::to('users');
    }

    /**
     * Remove the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        if ($user = $this->users->createModel()->find($id)) {
            $user->delete();
            Session::flash('message', 'Deleted Successfully.');
            return Redirect::to('users');
        }
        return Redirect::to('users');
    }

    /**
     * Show the form for updating profile.
     *
     * @param  int  $id
     * @return mixed
     */
    public function profile() {
        $user = Sentinel::getUser();
        return View::make('users.profile', compact('user'));
    }

    /**
     * Handle posting of the form for updating profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postProfile(\App\Http\Requests\ProfileUserPostRequest $request) {
        $user = Sentinel::getUser();
        $data = $request->except('roles', 'email');
        if (empty($data['password'])) {
            unset($data['password']);
        }
        $this->users->update($user, $data);
        Session::flash('message', 'Updated Successfully.');
        return Redirect::back();
    }

    public function login(Request $request) {
        return View::make('auth.login');
    }

    public function postLogin(\App\Http\Requests\LoginUserPostRequest $request) {
        try {
            $input = $request->all();
            $remember = false;
            if (Sentinel::authenticate($input, $remember)) {
                return Redirect::intended('users');
            }
            $errors = ['IncorrectCredentials' => 'Invalid login or password.'];
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            $errors = ['NotActivated' => 'Account is not activated!'];
            return Redirect::back()
                            ->withInput()
                            ->withErrors($errors);
        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            $delay = $e->getDelay();
            $errors = ['Throttling' => "Your account is blocked for {$delay} second(s)."];
            return Redirect::back()
                            ->withInput()
                            ->withErrors($errors);
        }
        return Redirect::back()
                        ->withInput()
                        ->withErrors($errors);
    }

    public function logout() {
        Sentinel::logout();
        return Redirect::to(route('login'));
    }

}
