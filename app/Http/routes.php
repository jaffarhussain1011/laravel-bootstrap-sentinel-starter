<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
//    $routes = Route::getRoutes();
//    $permissions = [];
//    foreach ($routes as $route) {
//        $action = $route->getAction();
//        echo "<pre>";
//        if (array_key_exists('controller', $action)) {
//            $result = explode('@', $action['controller']);
//            $method = $result[1];
//            $controllerClass = $result[0];
//            $controller = explode("\\", $controllerClass);
//            // to separate the class name from the method
//            $permissions[$route->uri()] = $route->uri();
//        }
//    }
//    print_r($permissions);
})->name('home');
Route::get('login', ['as' => 'login', 'uses' => 'AuthController@login', 'middleware' => 'NOLOGIN']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@postLogin', 'middleware' => 'NOLOGIN']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

Route::group(['prefix' => 'users', 'middleware' => 'ISLOGEDIN'], function() {
    Route::get('/', ['as' => 'users', 'uses' => 'UsersController@index', 'middleware' => 'ACL:users.view']);
    Route::get('create', ['as' => 'users.create', 'uses' => 'UsersController@create', 'middleware' => 'ACL:users.create']);
    Route::post('create', ['as' => 'users.create', 'uses' => 'UsersController@store', 'middleware' => 'ACL:users.store']);
    Route::get('update/{id}', ['as' => 'users.update', 'uses' => 'UsersController@edit', 'middleware' => 'ACL:users.update']);
    Route::post('update/{id}', ['as' => 'users.update', 'uses' => 'UsersController@update', 'middleware' => 'ACL:users.update']);
    Route::get('delete/{id}', ['as' => 'users.delete', 'uses' => 'UsersController@delete', 'middleware' => 'ACL:users.delete']);

    Route::get('profile', ['as' => 'users.profile', 'uses' => 'UsersController@profile']);
    Route::post('profile', ['as' => 'users.profile', 'uses' => 'UsersController@postProfile']);
});

Route::group(['prefix' => 'roles', 'middleware' => 'ISLOGEDIN'], function() {
    Route::get('/', ['as' => 'roles', 'uses' => 'RolesController@index', 'middleware' => 'ACL:roles.view']);
    Route::get('create', ['as' => 'roles.create', 'uses' => 'RolesController@create', 'middleware' => 'ACL:roles.create']);
    Route::post('create', ['as' => 'roles.create', 'uses' => 'RolesController@store', 'middleware' => 'ACL:roles.store']);
    Route::get('update/{id}', ['as' => 'roles.update', 'uses' => 'RolesController@edit', 'middleware' => 'ACL:roles.update']);
    Route::post('update/{id}', ['as' => 'roles.update', 'uses' => 'RolesController@update', 'middleware' => 'ACL:roles.update']);
    Route::get('delete/{id}', ['as' => 'roles.delete', 'uses' => 'RolesController@delete', 'middleware' => 'ACL:roles.delete']);
});
