<?php

namespace App\Analytics;

use Sentinel;
/**more complex functions will come in future**/
class ACL
{
    public static function has($permissions)
    {
        if (Sentinel::check() && (Sentinel::hasAnyAccess(["admin","superadmin"]) || Sentinel::hasAccess($permissions))) {
            return true;
        }
        return false;
    }
    public static function hasAny($permissions)
    {
        if (Sentinel::check() && (Sentinel::hasAnyAccess(["admin","superadmin"]) || Sentinel::hasAnyAccess($permissions))) {
            return true;
        }
        return false;
    }
    public static function isLoggedIn() {
        if(Sentinel::check()){
            return true;
        }
        return false;
    }
}
