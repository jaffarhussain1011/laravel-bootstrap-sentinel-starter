## Laravel 5 Sentinel Demo ##

**Laravel 5 Sentinel Demo** is a tutorial for Laravel ACL System with Sentinel

### Installation ###

* `git clone https://bitbucket.org/jaffarhussain1011/laravel-bootstrap-sentinel-starter.git`
* `cd laravel-bootstrap-sentinel-starter`
* `composer install`
* `php artisan key:generate`
* Adjust your database settings *.env*
* `php artisan migrate ` to create tables
* `php artisan db:seed ` to populate tables

### Include ###

* [Bootstrap](http://getbootstrap.com) for CSS and jQuery plugins
* [Font Awesome](http://fortawesome.github.io/Font-Awesome) for the nice icons

### Features ###

* Home page
* Sentinel Authentication (login, logout)
* Sentinel Users management(List,Create,Update,Delete)
* Sentinel Role management(List,Create,Update,Delete)
* User roles : admin (all access),superadmin(all access), manager (create , edit , delete users, View roles access),user(login,logout,profile access)

### Packages included ###

* laravelcollective/html
* cartalyst/sentinel: 2.0.*


### Default users and roles seeded ###

To test application the database is seeding with users :

* Super Administrator : email = superadmin@app.dev, password = 123
* Administrator : email = admin@app.dev, password = 123
* Manager : email = manager@app.dev, password = 123
* Standard User : email = user@app.dev, password = 123