<?php

use Illuminate\Database\Seeder;
//use Sentinel;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //admin
        $credentials = [
            'email' => 'admin@app.dev',
            'last_name'=> 'Admin',
            'password' => '123',
        ];
        $adminUser = Sentinel::registerAndActivate($credentials);
        $adminRole = Sentinel::findRoleBySlug('admin');
        $adminUser->roles()->attach($adminRole);
        //super admin
        $credentials = [
            'email' => 'superadmin@app.dev',
            'last_name'=> 'SuperAdmin',
            'password' => '123',
        ];
        $superAdminUser = Sentinel::registerAndActivate($credentials);
        $superAdminRole = Sentinel::findRoleBySlug('superadmin');
        $superAdminUser->roles()->attach($superAdminRole);
        //manager
        $credentials = [
            'email' => 'manager@app.dev',
            'last_name'=> 'Manager',
            'password' => '123',
        ];
        $managerUser = Sentinel::registerAndActivate($credentials);
        $managerRole = Sentinel::findRoleBySlug('manager');
        $managerUser->roles()->attach($managerRole);

        //standard user
        $credentials = [
            'email' => 'user@app.dev',
            'last_name'=> 'User',
            'password' => '123',
        ];
        $User = Sentinel::registerAndActivate($credentials);
        $userRole = Sentinel::findRoleBySlug('user');
        $User->roles()->attach($userRole);
        
    }

}
