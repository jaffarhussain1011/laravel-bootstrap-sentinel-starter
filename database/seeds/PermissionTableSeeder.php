<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        DB::table('permissions')->insert([
        [
            'name' => 'admin'
        ],[
            'name' => 'superadmin'
        ],[
            'name' => 'users.view'
        ],[
            'name' => 'users.create'
        ],[
            'name' => 'users.update'
        ],[
            'name' => 'users.delete'
        ],[
            'name' => 'roles.view'
        ],[
            'name' => 'roles.create'
        ],[
            'name' => 'roles.update'
        ],[
            'name' => 'roles.delete'
        ],
            ]);
    }
}
