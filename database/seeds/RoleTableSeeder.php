<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        //admin role
        $adminRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        $adminRole->updatePermission('admin', true, true)->save();
        //super admin role
        $superAdminRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Super Admin',
            'slug' => 'superadmin',
        ]);
        $superAdminRole->updatePermission('superadmin', true, true)->save();
        //manager role can  view,update,delete users and only view roles
        $managerRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Manager',
            'slug' => 'manager',
        ]);
        $managerRole->updatePermission('users.view', true, true)->save();
        $managerRole->updatePermission('users.update', true, true)->save();
        $managerRole->updatePermission('users.delete', true, true)->save();
        $managerRole->updatePermission('roles.view', true, true)->save();
        //standard user role
        $userRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Standard User',
            'slug' => 'user',
        ]);
    }
}
