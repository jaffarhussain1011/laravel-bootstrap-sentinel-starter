<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{!! Config::get('settings.appname') !!} </title>
        <link href="{{asset("font-awesome/css/font-awesome.css")}}" rel="stylesheet" type="text/css">
        <link href="{{asset("css/app.css")}}" rel="stylesheet" type="text/css">
        <script src="{{asset("js/app.js")}}"></script>
        
    </head>
    <body>
        @include("menu.top")
        <div class="container">
            <div class="content">
                <div class="title text-center">{!! Config::get('settings.appname') !!}</div>
            </div>
        </div>
    </body>
</html>
