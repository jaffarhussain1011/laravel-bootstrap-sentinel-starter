@extends('layouts.login')
@section('title')
Login
@endsection
{{-- Page content --}}
@section('content')

<div class="page-header">
    Login
</div>
<div class="page-body">
    <div>
        @if($errors->has('IncorrectCredentials') || $errors->has('NotActivated') || $errors->has('Throttling'))
        <ul class="alert alert-danger list-unstyled">
            @if($errors->has('IncorrectCredentials'))
            <li>
                {{{ $errors->first('IncorrectCredentials', ':message') }}}
            </li>
            @endif
            @if($errors->has('NotActivated'))
            <li>
                {{{ $errors->first('NotActivated', ':message') }}}
            </li>
            @endif
            @if($errors->has('Throttling'))
            <li>
                {{{ $errors->first('Throttling', ':message') }}}
            </li>
            @endif
        </ul>
        @endif
    </div>
    {!! Form::open(array('url' => url("login"), 'method' => 'POST')) !!}
    <div class="form-group{{ $errors->first('email', ' has-error') }}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email',Input::old('email'),["class"=>"form-control","placeholder"=> "Enter the user email."]) !!}
        <span class="help-block">{{{ $errors->first('email', ':message') }}}</span>
    </div>

    <div class="form-group{{ $errors->first('password', ' has-error') }}">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ["class"=>"form-control","placeholder"=> "Enter the user password."]); !!}

        <span class="help-block">{{{ $errors->first('password', ':message') }}}</span>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>

    {!! Form::close() !!}

</div>    
@stop
