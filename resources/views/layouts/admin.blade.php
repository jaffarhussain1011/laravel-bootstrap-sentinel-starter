<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{!! Config::get('settings.appname') !!} - @yield('title')</title>
        <link href="{{asset("font-awesome/css/font-awesome.css")}}" rel="stylesheet" type="text/css">
        <link href="{{asset("css/app.css")}}" rel="stylesheet" type="text/css">
        <script src="{{asset("js/app.js")}}"></script>
    </head>
    <body>
        @include("menu.top")
        @section('sidebar')

        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>