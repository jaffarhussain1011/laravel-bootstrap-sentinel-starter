@extends('layouts.admin')
@section('title')
Update Role
@endsection
{{-- Page content --}}
@section('content')

<div class="page-header">
    <h1>{{ $mode == 'create' ? 'Create Role' : 'Update Role' }} 
        <small>{{ $mode === 'update' ? $role->name : null }}</small>
    </h1>
</div>
<div class="page-body">
    {!! Form::open(array('url' => url("roles/update",["id"=>$role->id]), 'method' => 'POST')) !!}
    <div class="form-group{{ $errors->first('name', ' has-error') }}">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',Input::old('name', $role->name),["class"=>"form-control","placeholder"=> "Enter the name."]) !!}
        <span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
    </div>
    <div class="form-group{{ $errors->first('slug', ' has-error') }}">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::text('slug',Input::old('slug', $role->slug),["class"=>"form-control","placeholder"=> "Enter the slug"]) !!}
        <span class="help-block">{{{ $errors->first('slug', ':message') }}}</span>

    </div>
    <div class="form-group{{ $errors->first('permissions', ' has-error') }}">
        {!! Form::label('permissions[]', 'Permissions') !!}
        {!! Form::select('permissions[]', $permissions, array_keys($role->permissions), ['class' => 'form-control', 'multiple'=>'true']) !!}
        <span class="help-block">{{{ $errors->first('permissions', ':message') }}}</span>

    </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
    {!! Form::close() !!}
</div>    
@stop
