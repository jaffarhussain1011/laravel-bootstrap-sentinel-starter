@extends('layouts.admin')
@section('title')
Roles
@endsection
{{-- Page content --}}
@section('content')
<div class="page-header">
    <h1>Roles <span class="pull-right"><a href="{{ URL::to('roles/create') }}" class="btn btn-warning">Create</a></span></h1>
</div>
<div class="page-body">
    <div>
        @if(Session::has('message'))
        <ul class="alert alert-success list-unstyled">
            <li>
                {{{ Session::get('message') }}}
            </li>
        </ul>
        @endif
    </div>   
    @if ($roles->count())
    <div class="pull-right">
        {!! $roles->render() !!}
    </div>
    <table class="table table-bordered">
        <thead>
        <th class="col-lg-6">Name</th>
        <th class="col-lg-4">Slug</th>
        <th class="col-lg-4">Permissions</th>
        <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($roles as $role)
            <tr>
                <td>{{ $role->name }}</td>
                <td>{{ $role->slug }}</td>
                <td>
                    {!! http_build_query($role->permissions,'',', ') !!}
                </td>
                <td>
                    <a class="" href="{{ URL::to("roles/update/{$role->id}") }}">
                        <span class="fa fa-pencil"></span>
                    </a>
                    &nbsp;
                    <a class="" href="{{ URL::to("roles/delete/{$role->id}/delete") }}">
                        <span class="fa fa-trash"></span>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {!! $roles->render() !!}
    </div>
    @else
    <div class="well">
        Nothing to show here.
    </div>
    @endif
</div>

@stop
