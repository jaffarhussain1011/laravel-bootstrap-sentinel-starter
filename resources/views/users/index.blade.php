@extends('layouts.admin')
@section('title')
Users
@endsection
{{-- Page content --}}
@section('content')
<div class="page-header">
    <h1>Users <span class="pull-right"><a href="{{ URL::to('users/create') }}" class="btn btn-warning">Create</a></span></h1>
</div>
<div class="page-body">
    <div>
        @if(Session::has('message'))
        <ul class="alert alert-success list-unstyled">
            <li>
                {{{ Session::get('message') }}}
            </li>
        </ul>
        @endif
    </div>    
    @if ($users->count())
    <div class="pull-right">
        {!! $users->render() !!}
    </div>
    <table class="table table-bordered">
        <thead>
        <th class="col-lg-6">Name</th>
        <th class="col-lg-4">Email</th>
        <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if(APPACL::has("users.update"))
                    <a class="" href="{{ URL::to("users/update/{$user->id}") }}">
                        <span class="fa fa-pencil"></span>
                    </a>
                    @endif
                    &nbsp;
                    @if(APPACL::has("users.delete"))
                    <a class="" href="{{ URL::to("users/delete/{$user->id}") }}">
                        <span class="fa fa-trash"></span>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {!! $users->render() !!}
    </div>
    @else
    <div class="well">
        Nothing to show here.
    </div>
@endif
</div>

@stop
