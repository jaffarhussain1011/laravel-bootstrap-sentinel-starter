@extends('layouts.admin')
@section('title')
Create User
@endsection
{{-- Page content --}}
@section('content')

<div class="page-header">
    <h1>{{ $mode == 'create' ? 'Create User' : 'Update User' }} 
        <small>{{ $mode === 'update' ? $user->name : null }}</small>
    </h1>
</div>
<div class="page-body">
    <div>
        @if(Session::has('message'))
        <ul class="alert alert-success list-unstyled">
            <li>
                {{{ Session::get('message') }}}
            </li>
        </ul>
        @endif
    </div>
    {!! Form::open(array('url' => url("users/create"), 'method' => 'POST')) !!}
    <div class="form-group{{ $errors->first('first_name', ' has-error') }}">
        {!! Form::label('first_name', 'First Name') !!}
        {!! Form::text('first_name',Input::old('first_name', $user->first_name),["class"=>"form-control","placeholder"=> "Enter the user first_name."]) !!}
        <span class="help-block">{{{ $errors->first('first_name', ':message') }}}</span>

    </div>

    <div class="form-group{{ $errors->first('last_name', ' has-error') }}">
        {!! Form::label('last_name', 'Last Name') !!}
        {!! Form::text('last_name',Input::old('last_name', $user->last_name),["class"=>"form-control","placeholder"=> "Enter the user last_name."]) !!}
        <span class="help-block">{{{ $errors->first('last_name', ':message') }}}</span>

    </div>

    <div class="form-group{{ $errors->first('email', ' has-error') }}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email',Input::old('email', $user->email),["class"=>"form-control","placeholder"=> "Enter the user email."]) !!}

        <span class="help-block">{{{ $errors->first('email', ':message') }}}</span>

    </div>

    <div class="form-group{{ $errors->first('password', ' has-error') }}">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ["class"=>"form-control","placeholder"=> "Enter the user password."]); !!}

        <span class="help-block">{{{ $errors->first('password', ':message') }}}</span>

    </div>

    <button type="submit" class="btn btn-default">Submit</button>

    {!! Form::close() !!}

</div>    
@stop
